import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Container from "react-bootstrap/Container";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

class MyForm extends React.Component {
  constructor(props) {
    super(props);
//    this.state = { username: '' };
    this.state = { resultados: 0 };
  }
    

	funcion = (event) => {
	this.setState({funcion: event.target.value});
//    this.setState({username: this.state.number1});
	    	axios.get('http://taller03.herokuapp.com/' + this.state.funcion + '/' + this.state.numero1 + '/' + this.state.numero2)
		.then(response => this.setState({resultados: response.data.result}))

  } 
  
  myChangeHandler1 = (event) => {
    this.setState({numero1: event.target.value});
//    this.setState({username: this.state.number1}); 
 
	} 

 myChangeHandler2 = (event) => {
	this.setState({numero2: event.target.value});
//    this.setState({username: this.state.number1}); 
 
	} 
	
  render() {  
    return (
	
	<Container>

	
	<div class='calculator card'>
	
<center><h1><b>Consulta:</b></h1></center>
<input type='text' class='calculator-screen z-depth-1' value={'http://taller03.herokuapp.com/' + this.state.funcion + '/' + this.state.numero1 + '/' + this.state.numero2} disabled />	
	
<center><h1><b>Parametro 1:</b></h1></center>
<input type='text' class='calculator-input z-depth-1' id='parametro' onChange={this.myChangeHandler1} value={this.state.numero1}/>

<center><h1><b>Parametro 2:</b></h1></center>
<input type='text' class='calculator-input z-depth-1' id='parametro' onChange={this.myChangeHandler2} value={this.state.numero2}/>

<center><h1><b>Resultado:</b></h1></center>
<input type='text' class='calculator-screen z-depth-1' value={this.state.resultados} disabled />


<div class='calculator-keys'>

  <button type='button' class='operator btn btn-info' value='menor' onClick={this.funcion}>Evaluar menor</button>


</div>
</div>
	
	</Container>
     
    );
  }
}

ReactDOM.render(<MyForm />, document.getElementById('root'));

